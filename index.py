import bottle
from bottle import TEMPLATE_PATH, jinja2_view, route, install, debug, static_file, run, abort, redirect, request, response
from live_stylus import ConvStylus
import sys, os
import sqlite3
import functools

#TEMPLATE_PATH.append("templates")
app = bottle.default_app()             # or bottle.Bottle() if you prefer
view = functools.partial(jinja2_view, template_lookup=['templates'])

app.config['autojson']    = False      # Turns off the "autojson" feature
app.config['sqlite.db']   = ':memory:' # Tells the sqlite plugin which db to use
app.config['myapp.param'] = 'value'    # Example for a custom config value.

# Change many values at once
app.config.update({
    'autojson': False,
    'sqlite.db': ':memory:',
    'myapp.param': 'value'
})

# Add default values
app.config.setdefault('myapp.param2', 'some default')
app.config.load_config('/settings.ini')
# Receive values
param  = app.config['myapp.param']
param2 = app.config.get('myapp.param2', 'fallback value')


@app.hook('config')
def on_config_change(key, value):
  print key, value

@app.route('/todo')
def todo_list():
    conn = sqlite3.connect('todo.db')
    c = conn.cursor()
    c.execute("SELECT id, task FROM todo WHERE status LIKE '1'")
    result = c.fetchall()
    return {'data': str(result)}

@app.route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='')

# Static Routes
@app.route('/static/js/<filename:re:.*\.js>')
def javascripts(filename):
    return static_file(filename, root='static/js')

@app.route('/static/css/<filename:re:.*\.css>')
def stylesheets(filename):
    return static_file(filename, root='static/css')

@app.route('/static/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
    return static_file(filename, root='static/images')

@app.route('/static/fonts/<filename:re:.*\.(eot|ttf|woff|svg)>')
def fonts(filename):
    return static_file(filename, root='static/fonts')

@app.route('/login')
def login():
    return '''
        <form action="/login" method="post">
            Username: <input name="username" type="text" />
            Password: <input name="password" type="password" />
            <input value="Login" type="submit" />
        </form>
    '''

@app.route('/login', method='POST')
def do_login():
    username = request.forms.get('username')
    password = request.forms.get('password')
    if check_login(username, password):
        return "<p>Your login information was correct.</p>"
    else:
        return "<p>Login failed.</p>"

@app.route('/hello/:name')
def index(name='World'):
	response.set_cookie("name", name)
	return template('<b>Hello {{name}}</b>!', name=name)

@app.route('/my_ip')
def show_ip():
    ip = request.environ.get('REMOTE_ADDR')
    # or ip = request.get('REMOTE_ADDR')
    # or ip = request['REMOTE_ADDR']
    return template("Your IP is: {{ip}}", ip=ip)

@app.route('/')
@view('home.html')
def home():
    print 'app.config', app.config
    if request.cookies.name:
        return {'name':request.cookies.name}
    else:
        response.set_cookie("visited", "yes")
        return {'name':'test'}

@app.route('/restricted')
def restricted():
    abort(401, "Sorry, access denied.")

@app.route('/wrong/url')
def wrong():
    redirect("/right/url")

@app.route('/is_ajax')
def is_ajax():
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        return 'This is an AJAX request'
    else:
        return 'This is a normal request'   

@app.route('/forum')
def display_forum():
    forum_id = request.query.id
    page = request.query.page or '1'
    return template('Forum ID: {{id}} (page {{page}})', id=forum_id, page=page)

if __name__ == "__main__":
    ConvStylus('/home/magrabbit/Project/stylus/static/css')
    debug(True)
    app.run(host='0.0.0.0', port=8080)

